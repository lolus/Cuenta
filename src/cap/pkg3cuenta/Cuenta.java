/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cap.pkg3cuenta;

import javax.swing.JOptionPane;

/**
 *
 * @author Vicente
 */
public class Cuenta {
    private String nombre; // variable de instancia
    private double saldo; // variable de instancia
    //Constructor que inicializa las variables
    Cuenta(String nombre, double saldo){
            this.nombre = nombre;
        // verifica que el saldo sea mayor que 0.0; 
            if (saldo > 0.0) // si el saldo es válido
                 this.saldo = saldo; // lo asigna a la variable de instancia saldo
        }
     // método que deposita una cantidad válida al saldo
        public void depositar(double montoDeposito)
        {//El monto del deposio tiene que ser mayor a 0.0para sumarlo al saldo
            if (montoDeposito > 0.0) 
                saldo = saldo + montoDeposito; 
        }
        // método que devuelve el saldo de la cuenta
        public double getSaldo()
        {
            return saldo;
        }

        // método que establece el nombre
        public void setNombre(String nombre)
        {
            this.nombre = nombre;
        }

        // método que devuelve el nombre
        public String getNombre()
        {
            return nombre; 
        } 
         // método que retira dinero de la cuenta
        public void retirar(double x)
        {
            if(x > saldo){
                System.out.println("El monto a retirar excede el saldo de la cuenta");
            }
            else{
                saldo = saldo - x;
            }
        }
       //Método que muestra los datos de la cuenta
        public void mostrarCuenta(){
            System.out.printf("Saldo de %s: $%.2f%n",getNombre(),getSaldo());
        }
    // fin de la clase Cuenta
}

   
   
    
