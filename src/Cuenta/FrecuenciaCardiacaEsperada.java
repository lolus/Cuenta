/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cuenta;

/**
 *
 * @author Vicente
 */
public class FrecuenciaCardiacaEsperada {
    //DEclaramos nuestras variables de instancia
    private String nombre;
    private String apellido;
    private int dia;
    private int mes;
    private int año;
    
    //constructor que inicializa cada variable
  FrecuenciaCardiacaEsperada(String n, String ap, int d, int m, int a)
        {
            this.nombre = n;
            this.apellido = ap;
            this.dia = d;
            this.mes = m;
            this.año = a;
        }
  //metodos para establecer y obtener los valores de nuestras variables

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String n) {
        this.nombre = n;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String ap) {
        this.apellido = ap;
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int d) {
        this.dia = d;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int m) {
        this.mes = m;
    }

    public int getAño() {
        return año;
    }

    public void setAño(int a) {
        this.año = a;
    }
    //metodo para calcular la edad
    public int calculaEdad(int año, int mes)
        {
        int e=-1;
        for(int i=getAño() ; i<año ; i++)
        {
        e++;
        }
        if(mes > getMes())
        e++;
        return e;
        }
     //método que calcula la frecuencia cardiaca máxima
        public int frecuenciaCardiacaMaxima(int edad)
        {
            return 220-edad;
        }
        //método que calcula la frecuencia cardiaca esperada en el 62.5%
       
        public double frecuenciaCardiacaEsperada(int fe){
         return fe*0.625;
        }
  public void mostrarDatosFCE(int fcm, double fe, int edad)
        {
            System.out.printf("%s %s%n",nombre, apellido);
            if(mes>0 && mes<10)
                System.out.printf("Fecha de nacimiento: %d / 0%d / %d%n", dia, mes, año);
            
            System.out.printf("Edad: %d%n",edad);
            System.out.printf("Tu frecuencia cardiaca máxima es: %d%n",fcm);
            System.out.printf("Tu frecuencia cardiaca esperada es: %.2f%n",fe);
        }
    
   
    
}
