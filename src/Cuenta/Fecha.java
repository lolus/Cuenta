/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cuenta;

/**
 *
 * @author Vicente
 */
public class Fecha {
    //Declaramos variables de instancia
    private int mes;
    private int dia; 
    private int año;
    
    //Constructor que inicializa las variales
    
    Fecha(int m, int d, int a){
        this.mes=m;
        this.dia=d;
        this.año=a;
    }
    //metodo para establecer y obtener valores d nuestras variables

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public int getAño() {
        return año;
    }

    public void setAño(int año) {
        this.año = año;
    }
     public void mostrarFecha(){
        if(getMes()>0 && getMes()<12)
            System.out.printf("%d / 0%d / %d%n", getDia(), getMes(), getAño());
     
    }
    
    
}
