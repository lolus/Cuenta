/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cuenta;

/**
 *
 * @author Vicente
 */
public class prueba_Empleado {
    public static void main(String[] args)
    {
       Empleado empleado1 = new Empleado ("Sick Mateo", "Lopez",19000.00);
       Empleado empleado2 =new Empleado ("Luna","Lopez",26000.00);
       System.out.printf("Empleado 1: %s %s%nSalario Mesual: %.2f%n", 
         empleado1.getPrimerNombre(), empleado1.getApellidoPaterno(), empleado1.getSalarioMensual());
      System.out.printf("Salario anual: %.2f%n", empleado1.getSalarioMensual()*12);
      System.out.printf("Empleado 2: %s %s%nSalario Mesual: %.2f%n", 
       empleado2.getPrimerNombre(), empleado2.getApellidoPaterno(), empleado2.getSalarioMensual());
      System.out.printf("Salario anual: %.2f%n", empleado2.getSalarioMensual()*12);
      System.out.println("Se hace un aumento del %10 con respecto a los salarios");
      System.out.printf("Salario anual del empleado1: %.2f%n", empleado1.getSalarioMensual()*1.1*12);
      System.out.printf("Salario anual del empleado2: %.2f%n", empleado2.getSalarioMensual()*1.1*12);
    }
}
