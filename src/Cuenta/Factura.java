/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cuenta;

/**
 *
 * @author Vicente
 */
public class Factura {
    //Definimos nuestras variables de instancias.
    private String numero; 
    private String descripcion; 
    private int cantidad; 
    private double precio; 
   
    // Constructor de factura que recibe 4 parámetros para inicializar.
    Factura(String num, String des, int cant, double p){
        this.numero = num;
        this.descripcion = des;
        if(cant>0)
            this.cantidad = cant;
        else
            this.cantidad = 0;
        if(p>0)
            this.precio = p;
        else 
            this.precio = 0.00;
    }
    //Métodos que obtienen y establecen valores de nuestras variables de instancia.
    public String getNumero() {
        return numero;
    }

    public void setNumero(String num) {
        this.numero = num;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String des) {
        this.descripcion = des;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cant) {
         if(cant>0)
                this.cantidad = cant;
            else
                this.cantidad = 0;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double p) {
        if(p>0)
                this.precio = p;
            else 
                this.precio = 0.00;
        
    }
    //método que obtiene el total de la factura
        public double obtenerMontoFactura(){
            double mF = getPrecio()*getCantidad();
            return mF;
        } 
    
    
}
