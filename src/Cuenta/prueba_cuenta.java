/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cuenta;

import java.util.Scanner;

/**
 *
 * @author Vicente
 */
public class prueba_cuenta {
    public static void main(String[] args)
        {
         Cuenta cuenta1 = new Cuenta("Jane Green", 50.00);
         Cuenta cuenta2 = new Cuenta("John Blue", -7.53);
         // muestra el saldo inicial de cada objeto
         cuenta1.mostrarCuenta();
         cuenta2.mostrarCuenta();
         // crea un objeto Scanner para obtener valores desde el teclado
         Scanner entrada = new Scanner(System.in);
         System.out.print("Escriba el monto para depositar a cuenta1: "); 
         double montoDeposito = entrada.nextDouble();
         System.out.printf("%nsumando %.2f al saldo de cuenta1%n%n",montoDeposito);
         cuenta1.depositar(montoDeposito); // suma al saldo 
         // muestra los saldos de ambas cuentas
         cuenta1.mostrarCuenta();
         cuenta2.mostrarCuenta();
         System.out.print("Escriba el monto para depositar a cuenta2: "); 
         montoDeposito = entrada.nextDouble();
         System.out.printf("%nsumando %.2f al saldo de cuenta2%n%n",montoDeposito);
         cuenta2.depositar(montoDeposito); // suma al saldo 
         // muestra los saldos de ambas cuentas
         cuenta1.mostrarCuenta();
         cuenta2.mostrarCuenta();
         //pide dinero para retirar
         System.out.println("¿Cuanto dinero quieres retirar de cuenta1?");
         double montoRetiro = entrada.nextDouble();
         cuenta1.retirar(montoRetiro);
         cuenta1.mostrarCuenta();
         System.out.println("¿Cuanto dinero quieres retirar de cuenta2?");
         montoRetiro = entrada.nextDouble(); 
         cuenta2.retirar(montoRetiro);
         cuenta2.mostrarCuenta();
        } 
    
}
